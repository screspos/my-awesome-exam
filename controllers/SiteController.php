<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use app\models\Etapa;
use app\models\Ciclista;
use app\models\Puerto;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $ciclistas = new ActiveDataProvider([
            "query" => Ciclista::find()->select("dorsal,nombre")->where("edad BETWEEN 25 AND 35")->distinct(),
            "pagination" => [
                "pageSize" => 9
            ]
        ]);
        
        $etapas = new ActiveDataProvider([
           "query" => Etapa::find()->select("numetapa,kms")->where("salida<>llegada")->distinct(),
            "pagination" => [
                "pageSize" => 9
            ]
        ]);
        
        $puertos = new ActiveDataProvider([
            "query" => Puerto::find()->select("nompuerto,dorsal")->where("altura>1500"),
            "pagination" => [
                "pageSize" => 9
            ]
        ]);
        
        
        return $this->render('index',[
            'ciclistas' => $ciclistas,
            'ciclistas_campos' => ['dorsal','nombre'],
            'ciclistas_enunciado' => 'Los ciclistas cuya edad está entre 25 y'
                . '35 años mostrando únicamente el dorsal y el nombre del'
                . 'ciclista.',
            'ciclistas_resumen' => 'Ciclistas entre 25 y 30 años.',
            'etapas' => $etapas,
            'etapas_campos' => ['numetapa','kms'],
            'etapas_enunciado' => 'Las etapas no circulares mostrando sólo el '
                . 'número de etapa y la longitud de las mismas.',
            'etapas_resumen' => 'Etapas no circulares.',
            'puertos' => $puertos,
            'puertos_campos' => ['nompuerto','dorsal'],
            'puertos_enunciado' => 'Los puertos con altura mayor a 1500 metros '
                . 'figurando el nombre del puerto y el dorsal del ciclisata '
                . 'que lo ganó.',
            'puertos_resumen' => 'Puertos más altos que 1500m.',
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
