<?php
use yii\grid\GridView;

/** @var yii\web\View $this */

$this->title = 'Acceso a Datos';
?>
<div class="site-index">
    <h2 class="title">¡Las consultas de mi asombroso examen!</h2>
    
    <section class="background-light-blue">
        
        <div class="col-md-4">
            <h2>1</h2>
            <h4><?=$ciclistas_enunciado?></h4>
        </div>
        <div class="col-md-4">
            <h2>2</h2>
            <h4><?=$etapas_enunciado?></h4>
        </div>
        <div class="col-md-4">
            <h2>3</h2>
            <h4><?=$puertos_enunciado?></h4>
        </div>
        
    </section>
    <section class="consultas">
        
        <div class="col-md-4">
            <h5 class="resumen"><?=$ciclistas_resumen?></h5>
            <?= GridView::widget([ 
            'dataProvider'=> $ciclistas, 
            'columns' => $ciclistas_campos
            ])?>
        </div>
        <div class="col-md-4">
            <h5 class="resumen"><?=$etapas_resumen?></h5>
            <?= GridView::widget([ 
            'dataProvider'=>$etapas, 
            'columns' => $etapas_campos 
            ])?>
        </div>
        <div class="col-md-4">
            <h5 class="resumen"><?=$puertos_resumen?></h5>
            <?= GridView::widget([ 
            'dataProvider'=>$puertos,
            'columns' => $puertos_campos
            ])?>
        </div>
    </div>
</div>
